import { Component, OnInit } from '@angular/core';
import { UserServiceService } from './user-service.service'
import { User } from './models/User';
import { Company } from './models/Company';
import { Address } from './models/Address';
import { GeographicalLocation } from './models/GeographicalLocation';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'angular-exercise2';
  users = [];
  selectedUser:User;
  isSelected:boolean = false;

  constructor(private userService: UserServiceService) {

  }

  ngOnInit() {
    this.selectedUser = new User;
    this.selectedUser.id = -1;
    this.userService.getUsers().subscribe((users:any[]) => {
      users.forEach((item) => {
        let newUser = new User();
        newUser = item;
        this.users.push(newUser);
      });
      console.log(this.users);
    })
  }

  userSelectedHandler(user) {
    this.selectedUser = user;
    this.isSelected = true;
  }

  userUpdatedHandler(user) {
    this.isSelected = false;

    for(let i=0;i<this.users.length;i++) {
      if(this.users[i].id === user.id) {
        this.users[i] = user;
      }
    }
  }
}
