import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { User } from '../models/User';
import { UserServiceService } from '../user-service.service';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit, OnChanges {

  @Input() selectedUserId:number;
  @Output() userUpdated = new EventEmitter;

  selectedUser:User;

  constructor(private userService:UserServiceService) { }

  ngOnInit() {

  }

  ngOnChanges(changes: SimpleChanges) {
    console.log(changes);
    this.userService.getUser(this.selectedUserId).subscribe((result)=> {
      let user = new User();
      user = result;
      this.selectedUser = user;
    })
  }

  editButtonHandler() {
    console.log(this.selectedUser);
    this.userUpdated.emit(this.selectedUser);
  }

}
