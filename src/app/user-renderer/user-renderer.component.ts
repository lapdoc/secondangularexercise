import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-user-renderer',
  templateUrl: './user-renderer.component.html',
  styleUrls: ['./user-renderer.component.css']
})
export class UserRendererComponent implements OnInit {

  @Input() users;
  @Output() selectedUserEvent = new EventEmitter;

  constructor() { }

  ngOnInit() {
  }

  selectedUserHandler(user) {
    this.selectedUserEvent.emit(user);
  }

}
