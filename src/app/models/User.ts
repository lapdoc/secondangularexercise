import { Company } from './Company';
import { Address } from './Address';

export class User {

    id:number;
    name:string;
    username:string;
    email:string;
    phone:string;
    website:string;
    company:Company;
    address:Address;

    constructor() {

    }
}