import { GeographicalLocation } from './GeographicalLocation';

export class Address {

    street:string;
    suite:string;
    city:string;
    zipcode:string;
    geo: GeographicalLocation;

    constructor() {

    }
    
}